package com.example.kyurim.beeradvertiser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

public class FindBeerActivity extends AppCompatActivity {

    private BeerExpert advice = new BeerExpert();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);             // because we have overrided, this is how we tell, run super's onCreate as well.
        setContentView(R.layout.activity_find_beer);    // specify which layout to use.
    }

    public void onClickFindBeer(View view) {            // name of this method must match layout xml's Button object onClick value.
        Log.i("FindBeerActivity:---", "FIND BEER Button Pressed");


        TextView brands_TextView = (TextView) findViewById(R.id.brands_textview);   // TextView
        Spinner color_Spinner = (Spinner) findViewById(R.id.color_spinner);         // Spinner

        // Get Value from Spinner & Set it as TextView
        String spinnerString = (String) color_Spinner.getSelectedItem();            // Spinner - current value

        // get beer advice from BeerExpert
        List<String> brandsList = advice.getBrands(spinnerString);

        StringBuilder brandsFormatted = new StringBuilder();
        for (String brand : brandsList) {       // create a String with a brand in each line
            brandsFormatted.append(brand).append("\n");      // add the brand of beer & go to next line
        }

        // Display beers
        Log.i("-----Unformated: ", Arrays.toString(brandsList.toArray()));      // Unformatted List
        Log.i("-----Formated: ", brandsFormatted+"");                           // Formatted List
        brands_TextView.setText(brandsFormatted);       // set TextView with Formatted List
    }
}
